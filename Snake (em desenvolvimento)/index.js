window.onload = function(){

    var stage = document.getElementById('stage');
    var ctx = stage.getContext("2d"); //transformando o canvas em 2d

    document.addEventListener("keydown", keyPush);

    setInterval(game, 80);

    const speed = 1;

    var speedX = 0;
    var speedY = 0; //cobra
    var pontoX = 10;
    var pontoY = 10;

    var tp = 20; //tamanho da peça
    var qp = 20; //qtd de peças no tabuleiro

    var appleX = 15; //frutinha
    var appleY = 15;

    var trail = []; //rastro da cobra
    var tail = 5; //tamanho da cobra


    function game(){
        pontoX += speedX;
        pontoY += speedY;
        
        if(pontoX < 0){ //right
            pontoX = qp - 1;
        }
        if(pontoX > qp - 1){
            pontoX = 0;
        }
        if(pontoY < 0){
            pontoY = qp - 1;
        }
        if(pontoY > qp - 1){
            pontoY = 0
        }

    
        ctx.fillStyle = "black" //tela de jogo preta
        ctx.fillRect(0,0, stage.clientWidth, stage.height) //pintando toda a tela

        ctx.fillStyle = "red"
        ctx.fillRect(appleX*tp, appleY*tp, tp,tp)

        ctx.fillStyle = "green";
        for (var i = 0; i < trail.length; i++){

            ctx.fillRect(trail[i].x*tp, trail[i].y*tp, tp,tp);

            if(trail[i].x == pontoX && trail[i].y == pontoY){
                speedX = 0;
                speedY = 0;
                tail = 5;
            }
        }

            trail.push({x:pontoX, y:pontoY})
            while(trail.length > tail){ //tirando ultimo elemento da cobra
                trail.shift();
            }

            if(appleX == pontoX && appleY == pontoY){
                tail++;
                appleX = Math.floor(Math.random()*qp);
                appleY = Math.floor(Math.random()*qp)
            }

    }


    function keyPush(event){
        switch(event.keyCode){
            case 37: //Left
                speedX = -speed;
                speedY = 0;
                break;
            
            case 38: //UP
                speedX = 0;
                speedY = -speed;
                break;
            
            case 39: //RIGHT
                speedX = speed;
                speedY = 0;
                break;
            case 40: //Down
                speedX = 0;
                speedY = speed;
                break;

        }

    } 


}